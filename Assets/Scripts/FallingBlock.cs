using System.Collections;
using UnityEngine;

public class FallingBlock : MonoBehaviour
{
    public Vector2 speedMinMax;
    float speed;
    float visibleHight;

    void Start() {
        speedMinMax = new Vector2 ((float) 5, (float) 15);
        speed = Mathf.Lerp (speedMinMax.x, speedMinMax.y, Difficulty.GetDifficultyPercent());
        visibleHight = -Camera.main.orthographicSize - transform.localScale.y;
    }

    // Update is called once per frame
    void Update() {
        transform.Translate(Vector3.down * speed * Time.deltaTime);
        if (transform.position.y < visibleHight) {
            Destroy (gameObject);
        }
    }
}
