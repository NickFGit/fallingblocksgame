using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    public GameObject FallingBlock;
    public Vector2 secondsBetweenSpawnsMinMax;
    public Vector2 spawnSizeMinMax;
    public float spawnAngleMax = 10;
    float nextSpawnTime;
    Vector2 HalfScreen;
    // Start is called before the first frame update
    void Start()
    {
        spawnSizeMinMax = new Vector2 ((float) 0.5, (float) 2);
        secondsBetweenSpawnsMinMax = new Vector2((float) 1, (float) 0.1);
        HalfScreen = new Vector2(Camera.main.aspect * Camera.main.orthographicSize, Camera.main.orthographicSize);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextSpawnTime)
        {
            float secondsBetweenSpawns = Mathf.Lerp(secondsBetweenSpawnsMinMax.x, secondsBetweenSpawnsMinMax.y, Difficulty.GetDifficultyPercent());
            nextSpawnTime = Time.time + secondsBetweenSpawns;
            float spawnAngle = Random.Range (-spawnAngleMax, spawnAngleMax);
            float spawnSize = Random.Range (spawnSizeMinMax.x, spawnSizeMinMax.y);
            Vector2 spawnPosition = new Vector2(Random.Range(-HalfScreen.x, HalfScreen.x), HalfScreen.y + spawnSize);
            GameObject newBlock = (GameObject)Instantiate(FallingBlock, spawnPosition, Quaternion.Euler(Vector3.forward *spawnAngle));
            newBlock.transform.localScale = Vector2.one * spawnSize;
        }
    }
}
