using UnityEngine;
using System.Collections;

public static class Difficulty {
    public static float secondsToMaxDiff = 60;
    public static float GetDifficultyPercent() {
        return Mathf.Clamp01(Time.timeSinceLevelLoad / secondsToMaxDiff);
    }
}
