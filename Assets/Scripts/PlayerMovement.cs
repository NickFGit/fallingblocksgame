using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
    public event System.Action OnPlayerDeath;
    public float speed = 7;
    float HalfScreenWidth = 1f;

    // Start is called before the first frame update
    void Start() {
        float halfPlayerWidth = transform.localScale.x /2f;
        HalfScreenWidth = Camera.main.aspect * Camera.main.orthographicSize + halfPlayerWidth;
    }

    // Update is called once per frame
    void Update() {
        float inputX = Input.GetAxisRaw("Horizontal");
        float velocity = inputX * speed;
        transform.Translate(Vector2.right * velocity * Time.deltaTime);
        if (transform.position.x < -HalfScreenWidth) {
            transform.position = new Vector2 (HalfScreenWidth, transform.position.y);
        }
        if (transform.position.x > HalfScreenWidth) {
            transform.position = new Vector2 (-HalfScreenWidth, transform.position.y);
        }
    }

    void OnTriggerEnter2D(Collider2D triggerCollider) {
        if (triggerCollider.tag == "FallingBlock") {
            if (OnPlayerDeath != null) {
                OnPlayerDeath();
            }
            Destroy (gameObject);
        }
    }
}
